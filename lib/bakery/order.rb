class Order
  def initialize
    @items = []
  end

  def add(order_item)
    @items.push(order_item)
  end

  attr_reader :items

  def count
    @items.length
  end

  def display
    @items.each do |item|
      puts item[:count].to_s + ' ' + item[:product].code + ' $' + item[:price].to_s
    end
  end
end
