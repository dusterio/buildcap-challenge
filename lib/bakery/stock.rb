class Stock
  def initialize(inventory = {})
    @inventory = inventory
  end

  def add(product, count, price)
    if @inventory.key?(product)
      variants = @inventory[product].reject { |variant| variant[:count] == count }
    else
      variants = []
    end

    @inventory[product] = variants.push(count: count, price: price)
  end

  def inventory(product)
    @inventory[product]
  end

  def compose(code, count)
    product = get(code)
    variants = @inventory[product].sort_by { |variant| variant[:count] }
    choose_packs(product, count.to_i, variants.reverse)
  end

  def choose_packs(product, count, variants)
    possible_numbers = variants.collect { |variant| variant[:count] }
    combination = Tools.coin_change(count, possible_numbers)
    raise 'Unable to break cart into valid packs: ' + product.name if combination.nil?
    combination.map do |pack_count|
      price = variants.select { |variant| variant[:count] == pack_count }.first[:price]
      { product: product, count: pack_count, price: price }
    end
  end

  def get(code)
    @inventory.keys.each do |product|
      return product if product.code == code
    end

    raise 'Product code does not exist'
  end

  def has?(product)
    @inventory.key? product
  end
end
