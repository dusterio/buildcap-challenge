class Cart
  def initialize(items = {})
    @items = items
    @items.default = 0
  end

  def add(code, count = 1)
    @items[code] += count
  end

  def self.from_plaintext(text)
    cart = new

    text.each_line do |line|
      count, code = line.split(' ')
      cart.add(code, count.to_i)
    end

    cart
  end

  attr_reader :items

  def count
    @items.length
  end
end
