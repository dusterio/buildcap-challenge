class Product
  def initialize(name, code)
    raise unless code.is_a?(String)
    @name = name
    @code = code
  end

  attr_reader :name

  attr_reader :code

  def eql?(other)
    @code == other.code
  end

  def hash
    @code.hash
  end
end
