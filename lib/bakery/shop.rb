class Shop
  def initialize(stock)
    @stock = stock
  end

  def process_cart(cart)
    order = Order.new

    cart.items.each do |code, count|
      @stock.compose(code, count).each { |order_item| order.add(order_item) }
    end

    display(order.items)
  end

  def display(items)
    items = items.group_by { |item| item[:product].code }

    items.each do |code, category|
      # Overall for the product
      total_count = category.inject(0) { |sum, pack| sum + pack[:count] }
      total_price = category.inject(0) { |sum, pack| sum + pack[:price] }
      puts total_count.to_s + ' ' + code + ' $' + total_price.round(2).to_s

      # Breakdown into packs
      packs = category.group_by { |pack| pack[:count] }
      packs.each do |count, pack|
        puts "\t" + pack.length.to_s + ' x ' + count.to_s + ' $' + pack.first[:price].to_s
      end
    end
  end
end
