module Tools
  # One of the fastest methods for the classic coin change problem, from Paolo Bonzini
  # Found on http://rubyquiz.com/quiz154.html
  def coin_change(target, options = [])
    return nil if target < 0
    return nil if target != target.floor

    parents = Array.new(target + 1)
    parents[0] = 0
    worklist = [[0, 0]]

    while parents[target].nil? && !worklist.empty?
      base, starting_index = worklist.shift
      starting_index.upto(options.size - 1) do |index|
        coin = options[index]
        tot = base + coin
        if tot <= target && parents[tot].nil?
          parents[tot] = base
          worklist << [tot, index]
        end
      end
    end

    return nil if parents[target].nil?
    result = []

    while target > 0
      parent = parents[target]
      result << target - parent
      target = parent
    end

    result.sort!.reverse!
  end

  module_function :coin_change
end
