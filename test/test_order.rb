require 'minitest/autorun'

root = File.expand_path('../..', __FILE__)
require File.join(root, %w(lib bakery))

class TestOrder < Minitest::Test
  def setup
    @order = Order.new
  end

  def test_can_convert_valid_cart_to_an_order
    product = Product.new('Vegemite', 'VS5')
    @order.add(product: product, variant: { count: 1 })

    product = Product.new('Vegemite', 'VS5')
    @order.add(product: product, variant: { count: 1 })

    assert(@order.count)
  end
end
