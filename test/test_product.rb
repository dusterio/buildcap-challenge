require 'minitest/autorun'

root = File.expand_path('../..', __FILE__)
require File.join(root, %w(lib bakery))

class TestProduct < Minitest::Test
  def setup
    @product = Product.new('Vegemite', 'VS5')
  end

  def test_product_has_a_name_and_a_code
    assert(@product.name)
    assert(@product.code)
  end
end
