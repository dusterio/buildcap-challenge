require 'rubygems'
require 'bundler/setup'
require 'minitest/autorun'

root = File.expand_path('../..', __FILE__)
require File.join(root, %w(lib bakery))
require File.join(root, %w(lib tools))

class TestBakery < Minitest::Test
  def test_the_challenge
    vegemite = Product.new('Vegemite Scroll', 'VS5')
    muffin = Product.new('Blueberry Muffin', 'MB11')
    croissant = Product.new('Croissant', 'CF')

    stock = Stock.new
    shop = Shop.new(stock)

    stock.add(vegemite, 3, 6.99)
    stock.add(vegemite, 5, 8.99)

    stock.add(muffin, 2, 9.95)
    stock.add(muffin, 5, 16.95)
    stock.add(muffin, 8, 24.95)

    stock.add(croissant, 3, 5.95)
    stock.add(croissant, 5, 9.95)
    stock.add(croissant, 9, 16.99)

    assert(stock.has?(vegemite))
    assert(stock.has?(muffin))
    assert(stock.has?(croissant))

    test_cart = "10 VS5\n"\
  	  "14 MB11\n"\
  	  '13 CF'

    test_expected_output = "10 VS5 $17.98\n"\
  	  "\t2 x 5 $8.99\n"\
  	  "14 MB11 $54.8\n"\
  	  "\t1 x 8 $24.95\n"\
  	  "\t3 x 2 $9.95\n"\
  	  "13 CF $25.85\n"\
  	  "\t2 x 5 $9.95\n"\
  	  "\t1 x 3 $5.95\n"

    cart = Cart.from_plaintext(test_cart)
    assert_output(test_expected_output) { shop.process_cart(cart) }
  end
end
