require 'minitest/autorun'

root = File.expand_path('../..', __FILE__)
require File.join(root, %w(lib bakery))
require File.join(root, %w(lib tools))

class TestStock < Minitest::Test
  def setup
    @product = Product.new('Vegemite', 'VS5')
    @stock = Stock.new
  end

  def test_stock_can_add_a_product_pack
    count = 3
    price = 6.99
    @stock.add(@product, count, price)
    packs = @stock.inventory(@product)
    assert(packs.any? { |pack| pack[:count] == count && pack[:price] == price })
    assert(@stock.has?(@product))
  end

  def test_the_same_count_will_get_overwritten
    count = 3
    price = 6.99
    @stock.add(@product, count, price)

    price = 8.99
    @stock.add(@product, count, price)

    packs = @stock.inventory(@product)
    assert(packs.any? { |pack| pack[:count] == count && pack[:price] == price })
    assert(packs.count == 1, 'Old pack is still present')
  end
end
