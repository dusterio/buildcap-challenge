require 'minitest/autorun'

root = File.expand_path('../..', __FILE__)
require File.join(root, %w(lib bakery))

class TestCart < Minitest::Test
  def setup
    @cart = Cart.new
  end

  def test_can_push_a_product_to_order
    @cart.add('VS5')
    assert(@cart.count > 0)
  end

  def test_pushing_the_same_product_will_increase_the_counter
    @cart.add('VS5')
    assert(@cart.count > 0)

    @cart.add('VS5', 2)
    assert(@cart.count == 1)
    assert(@cart.items['VS5'] == 3)
  end
end
