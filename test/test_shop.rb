require 'minitest/autorun'

root = File.expand_path('../..', __FILE__)
require File.join(root, %w(lib bakery))

class TestShop < Minitest::Test
  def setup
    @stock = Stock.new
    @shop = Shop.new(@stock)
  end

  def test_cant_process_an_cart_with_non_existing_stock
    product = Product.new('Vegemite', 'VS5')
    @stock.add(product, 1, 5)

    product = Product.new('Muffin', 'MB11')

    cart = Cart.new
    cart.add(product.code, 1)

    assert_raises(Exception) { @shop.process_cart(cart) }
  end

  def test_can_process_a_valid_cart
    product = Product.new('Vegemite', 'VS5')
    @stock.add(product, 1, 5)

    another_product = Product.new('Muffin', 'MB11')
    @stock.add(another_product, 1, 5)

    cart = Cart.new
    cart.add(product.code, 1)
    cart.add(another_product.code, 2)
    assert(@shop.process_cart(cart))
  end
end
